// Package cache implements cache interface.
package cache

import "gitlab.com/tirava/image-previewer/internal/domain/entities"

// Cacher is the main interface for cache logic.
type Cacher interface {
	Add(item entities.CacheItem) (entities.CacheItem, error)
	Get(hash string) (entities.CacheItem, bool)
	Clear()
}
